

module "appCluster" {
  source                   = "./appCluster"
  unique_number            = "98"
  ssh_key_pub              = var.ssh_key_pub
  ssh_key                  = var.ssh_key
  worker_count             = 1
  linode_tags              = ["appbuzz02", "appcluster", "helloNIT"]
  linodeapikey             = var.linodeapikey
  kubernetes_user_name     = var.kubernetes_user_name
  kubernetes_user_password = var.kubernetes_user_password
  linode_username          = var.linode_username
  archrootpass             = var.archrootpass
}
