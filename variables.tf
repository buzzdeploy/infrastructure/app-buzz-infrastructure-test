
variable "linodeapikey" {
  type        = string
  description = "Linode ApiKey"
}

variable "linode_username" {
  type        = string
  description = "Username to be used as noted in the Linode profile"
}

variable "kubernetes_user_name" {
  type        = string
  description = "Username used for the kubernetes user"
}

variable "kubernetes_user_password" {
  type        = string
  description = "Password for the kubernetes user"
}

variable "archrootpass" {
  type        = string
  description = "Arch Linux Root Password"
}

variable "ssh_key_pub" {
  type        = string
  description = "SSH Public Key"
}

variable "ssh_key" {
  type        = string
  description = "SSH Private Key"
}